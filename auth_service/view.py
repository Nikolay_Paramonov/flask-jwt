from datetime import datetime, timedelta

import jwt
from flask import request, jsonify, make_response
from werkzeug.security import check_password_hash
from app import app

from models import User


@app.route('/token', methods=['POST'])
def generate_token():
    req = request.form

    if not req or not req.get('email') or not req.get('password'):
        return make_response(
            'Could not verify',
            401,
            {'WWW-Authenticate': 'Basic realm ="Login required!"'}
        )

    user = User.query \
        .filter_by(email=req.get('email')) \
        .first()

    if not user:
        return make_response(
            'Could not verify',
            401,
            {'WWW-Authenticate': 'Basic realm ="User does not exist!"'}
        )
    if check_password_hash(user.password, req.get('password')):
        token = jwt.encode({
            'user_id': user.id,
            'exp': datetime.utcnow() + timedelta(minutes=30)
        }, app.config['JWT_SECRET_KEY'], algorithm=app.config['JWT_ALGORITHM'])
        return jsonify({'token': token.decode('UTF-8')}), 201

    return make_response(
        'Could not verify',
        403,
        {'WWW-Authenticate': 'Basic realm ="Wrong Password!"'}
    )
