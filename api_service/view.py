from flask import jsonify

from app import app
from models import Book
from utils import token_required


@app.route('/books', methods=['GET'])
@token_required
def get_all_users():
    books = Book.query.all()
    output = []
    for book in books:
        output.append({
            'id': book.id,
            'title': book.title,
            'author': book.author
        })

    return jsonify({'books': output})
