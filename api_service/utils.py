from functools import wraps
from flask import request, jsonify

import jwt

from app import app


def token_required(func):
    @wraps(func)
    def decorated(*args, **kwargs):
        token = None
        if 'Authorization' in request.headers:
            token = request.headers['Authorization']
        if not token:
            return jsonify({'message': 'Token is missing!'}), 401

        try:
            jwt.decode(token, app.config['JWT_PUBLIC_KEY'], algorithms=app.config["JWT_ALGORITHM"])
        except:
            return jsonify({
                'message': 'Token is invalid!'
            }), 401
        return func(*args, **kwargs)

    return decorated
